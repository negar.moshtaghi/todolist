//
//  UserDefaults.swift
//  todoList
//
//  Created by Negar Moshtaghi on 3/4/24.
//

import Foundation

extension UserDefaults {
    func getIncremental(for key: String) -> Int {
        let order = integer(forKey: key) + 1
        setValue(order, forKey: key)
        return order
    }
}
