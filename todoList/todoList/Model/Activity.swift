//
//  Activity.swift
//  todoList
//
//  Created by Negar Moshtaghi on 3/3/24.
//

import Foundation
import SwiftData

@Model
final class Activity {
    var title: String
    var caption: String
    var done: Bool
    var order: Int

    init(title: String, caption: String, done: Bool) {
        self.title = title
        self.caption = caption
        self.done = done
        self.order = UserDefaults.standard.getIncremental(for: String(describing: Activity.self))
    }
}
