//
//  CheckListFilter.swift
//  todoList
//
//  Created by Negar Moshtaghi on 3/3/24.
//

enum CheckListFilter: Int, CaseIterable {
    case all
    case unchecked
    case checked

    var image: String {
        switch self {
        case .all:
            "checklist"
        case .unchecked:
            "checklist.unchecked"
        case .checked:
            "checklist.checked"
        }
    }

    func next() -> CheckListFilter {
        var next = rawValue + 1
        if next > CheckListFilter.allCases.count - 1 { next = 0 }
        return CheckListFilter(rawValue: next) ?? .all
    }
}
