//
//  todoListApp.swift
//  todoList
//
//  Created by Negar Moshtaghi on 3/3/24.
//

import SwiftUI
import SwiftData

@main
struct todoListApp: App {
    var body: some Scene {
        WindowGroup {
            ListView(viewModel: ListViewModel())
        }
        .modelContainer(for: Activity.self)
    }
}
