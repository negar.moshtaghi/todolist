//
//  CheckmarkView.swift
//  todoList
//
//  Created by Negar Moshtaghi on 3/3/24.
//

import SwiftUI

struct CustomCheckmark: ToggleStyle {
    func makeBody(configuration: Configuration) -> some View {
        Button(action: {
            configuration.isOn.toggle()
        }, label: {
            HStack {
                Image(systemName: configuration.isOn ? "checkmark.circle.fill" : "circle")
                    .foregroundStyle(configuration.isOn ? Color.green : Color.secondary)
                configuration.label
            }
        })
    }
}
