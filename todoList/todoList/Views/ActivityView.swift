//
//  ActivityView.swift
//  todoList
//
//  Created by Negar Moshtaghi on 3/3/24.
//

import SwiftUI

struct ActivityView: View {
    @State var activity: Activity

    var body: some View {
        HStack {
            Toggle("", isOn: $activity.done)
            .toggleStyle(CustomCheckmark())

            VStack {
                HStack {
                    Text(activity.title)
                        .font(.caption)
                        .foregroundStyle(.primary)
                    Spacer()
                }
                HStack {
                    Text(activity.caption)
                        .font(.caption2)
                        .foregroundStyle(.secondary)
                    Spacer()
                }
            }
        }
        .padding(5)
        .background(Color.gray.opacity(0.2))
        .clipShape(.buttonBorder)
    }
}

#Preview {
    ActivityView(activity: Activity(title: "title", caption: "caption", done: true))
}

