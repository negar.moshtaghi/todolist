//
//  ListView.swift
//  todoList
//
//  Created by Negar Moshtaghi on 3/3/24.
//

import SwiftUI
import SwiftData

fileprivate enum Constants {
    static let alertMessage = "Please enter title and description for your new activity."
    static let alertTitle = "New Activity"
}

struct ListView<ViewModel: ListViewModelProtocol>: View {

    @Query(sort: \Activity.order) private var activities: [Activity]
    @Environment(\.modelContext) private var modelContext
    @ObservedObject var viewModel: ViewModel

    var body: some View {
        HStack {
            List {
                ForEach(viewModel.filter(activities: activities)) { activity in
                    ActivityView(activity: activity)
                        .listRowSeparator(.hidden)
                }
                .onDelete(perform: deleteActivities)
                .onMove(perform: move)
            }
            .listStyle(.plain)
        }
        .overlay(alignment: .topTrailing) {
            Button(action: { viewModel.incrementFilter() }, label: {
                Image(systemName: viewModel.filterImage)
                    .font(.title)
                    .foregroundStyle(.white)
                    .padding()
                    .background(.blue)
                    .clipShape(.circle)
                    .padding()
            })
        }
        .overlay(alignment: .bottomTrailing) {
            Button(action: { viewModel.showNewActivity() }, label: {
                Text("+")
                    .font(.largeTitle)
                    .foregroundStyle(.white)
                    .padding()
                    .background(.blue)
                    .clipShape(.circle)
                    .padding()
            })
            .alert(Constants.alertTitle, isPresented: viewModel.newActivityAlert, actions: {
                TextField("Title", text: viewModel.newActivityTitle)
                TextField("Description", text: viewModel.newActivityCaption)
                Button("Create", action: { addActivity() })
                Button("Cancel", role: .cancel, action: { resetNewActivity() })
            }, message: {
                Text(Constants.alertMessage)
            })
        }
    }

    private func resetNewActivity() {
        viewModel.resetNewActivity()
    }

    private func addActivity() {
        withAnimation {
            viewModel.addActivity(modelContext: modelContext)
        }
    }

    private func deleteActivities(offsets: IndexSet) {
        withAnimation {
            viewModel.deleteActivities(offsets: offsets, filteredActivities: viewModel.filter(activities: activities), modelContext: modelContext)
        }
    }

    private func move(from source: IndexSet, to destination: Int) {
        withAnimation {
            viewModel.move(from: source, to: destination, filteredActivities: viewModel.filter(activities: activities), modelContext: modelContext)
        }
    }
}

#Preview {
    ListView(viewModel: ListViewModel())
}
