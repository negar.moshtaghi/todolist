//
//  ListViewModel.swift
//  todoList
//
//  Created by Negar Moshtaghi on 3/4/24.
//

import SwiftData
import SwiftUI

protocol ListViewModelProtocol: ObservableObject {
    func move(from source: IndexSet, to destination: Int, filteredActivities: [Activity], modelContext: ModelContext)
    func deleteActivities(offsets: IndexSet, filteredActivities: [Activity], modelContext: ModelContext)
    func addActivity(modelContext: ModelContext)
    func resetNewActivity()
    func incrementFilter()
    func showNewActivity()
    var newActivityAlert: Binding<Bool> { get }
    var newActivityTitle: Binding<String> { get }
    var newActivityCaption: Binding<String> { get }
    var filterImage: String { get }
    func filter(activities: [Activity]) -> [Activity]
}

class ListViewModel: ListViewModelProtocol {

    @Published var filter: CheckListFilter = .all
    @Published private var publishedNewActivityAlert: Bool = false
    @Published private var publishedNewActivityTitle: String = ""
    @Published private var publishedNewActivityCaption: String = ""

    var newActivityAlert: Binding<Bool> {
        Binding(get: { self.publishedNewActivityAlert }, set: { self.publishedNewActivityAlert = $0 })
    }
    var newActivityTitle: Binding<String> {
        Binding(get: { self.publishedNewActivityTitle }, set: { self.publishedNewActivityTitle = $0 })
    }
    var newActivityCaption: Binding<String> {
        Binding(get: { self.publishedNewActivityCaption }, set: { self.publishedNewActivityCaption = $0 })
    }
    var filterImage: String {
        filter.image
    }

    func incrementFilter() {
        filter = filter.next()
    }

    func showNewActivity() {
        publishedNewActivityAlert = true
    }

    func resetNewActivity() {
        publishedNewActivityTitle = ""
        publishedNewActivityCaption = ""
    }

    func addActivity(modelContext: ModelContext) {
        guard !publishedNewActivityTitle.isEmpty, !publishedNewActivityCaption.isEmpty else { return }
        let newActivity = Activity(title: publishedNewActivityTitle, caption: publishedNewActivityCaption, done: false)
        modelContext.insert(newActivity)
        resetNewActivity()
    }

    func deleteActivities(offsets: IndexSet, filteredActivities: [Activity], modelContext: ModelContext) {
        for index in offsets where filteredActivities.indices.contains(index) {
            modelContext.delete(filteredActivities[index])
        }
    }

    func move(from source: IndexSet, to destination: Int, filteredActivities: [Activity], modelContext: ModelContext) {
        if let source = source.last,
           filteredActivities.indices.contains(destination),
           filteredActivities.indices.contains(source)
        {
            let destinationDate = filteredActivities[destination].order
            filteredActivities[source].order = destinationDate - 1
            filteredActivities[destination].order = destinationDate + 1
        }
    }

    func filter(activities: [Activity]) -> [Activity] {
        switch filter {
        case .all:
            activities
        case .unchecked:
            activities.filter { !$0.done }
        case .checked:
            activities.filter { $0.done }
        }
    }
}
