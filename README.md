A TodoList app, available for iOS 17+, macOS 14+.

Feature list:
- create new task
- toggle done/undone
- filter by done/undone
- swipe to delete
- reorder

SwiftUI used for UI implementation, SwiftData used for data persistance, MVVM as design pattern.

future improvements:
- adding tests
